Gitlab API Notebooks
====================

Uses the [gitlab api](https://docs.gitlab.com/ee/api/).

Writing this was easier and more fun than changing the projects by hand.

Setup
-----

This requires a [Personal Access Token](https://docs.gitlab.com/ee/api/#personal-access-tokens).
Save the token in a file named `TOKEN` in the root of this project.

This uses pipenv to manage dependencies.
